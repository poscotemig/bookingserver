const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017";
const ObjectId = require("mongodb").ObjectId;
const jwt = require("jsonwebtoken");
const hotels = require("./hotel");
const JWT_SECRET =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImp0aSI6IjBkMDNjNTAzLWQ4ODMtNDAwOS1hMzUyLTBlZGY5MmMyZDZkMSIsImlhdCI6MTU1MzYxNjk0OCwiZXhwIjoxNTUzNjIwNTQ4fQ.1100vfpmEzzLpx_tz9tneaGrUvdgBCVZGw3KTI1rQu8";

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(async (req, res) => {
  const authToken = req.headers.authorization;
  if (
    !(
      (req.path == "/api/login" ||
        req.path == "/api/usuario" ||
        req.path == "/api/logout") &&
      req.method == "POST"
    ) &&
    authToken == undefined
  ) {
    res.status(401).send("Token de verificação é obrigatório");
  } else if (authToken != undefined) {
    try {
      jwt.verify(authToken, JWT_SECRET);
      var token = await db.collection("token").findOne({ token: authToken });
      if (token == null) {
        throw { message: "Token inválido!" };
      }
      req.next();
    } catch (error) {
      res.status(401).send(error.message);
    }
  } else {
    req.next();
  }
});

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
  if (err) {
    console.log(err);
  }
  db = client.db("booking");

  app.listen(3000, () => {
    console.log("Listening on port 3000");
  });
});

// Login Area
app.post("/api/login", (req, res) => {
  db.collection("usuario").findOne(
    { username: req.body.username },
    (err, result) => {
      if (err) {
        res.send(err);
      }
      if (result != null && result.password == req.body.password) {
        var userId = result._id;
        jwt.sign({ userId }, JWT_SECRET, (err, token) => {
          if (err) {
            res.send(err);
          }
          db.collection("token").insertOne({ token });
          res.send({ userId, token });
        });
      } else {
        res.status(401).send("Email ou senha incorretos!");
      }
    }
  );
});

app.post("/api/logout", (req, res) => {
  const authToken = req.headers.authorization;
  db.collection("token").findOneAndDelete(
    { token: authToken },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.sendStatus(200);
      }
    }
  );
});

// Token Area
app.get("/api/token", (req, res) => {
  db.collection("token")
    .find()
    .toArray((err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    });
});

app.delete("/api/token/:id", (req, res) => {
  db.collection("token").deleteOne(
    { _id: ObjectId(req.params.id) },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.sendStatus(200);
      }
    }
  );
});

// User Area
app.get("/api/usuario", (req, res) => {
  db.collection("usuario")
    .find()
    .toArray((err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    });
});

app.get("/api/usuario/:id", (req, res) => {
  db.collection("usuario").findOne(
    { _id: ObjectId(req.params.id) },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );
});

app.post("/api/usuario", (req, res) => {
  db.collection("usuario").findOne(
    { username: req.body.username },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        if (result == null) {
          db.collection("usuario").insertOne(req.body, (err, result) => {
            if (err) {
              res.send(err);
            } else {
              res.send(req.body);
            }
          });
        } else {
          res.status(401).send("Usuário já existente!");
        }
      }
    }
  );
});

app.delete("/api/usuario/:id", (req, res) => {
  db.collection("usuario").deleteOne(
    { _id: ObjectId(req.params.id) },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.sendStatus(200);
      }
    }
  );
});

//Hotel Area
app.get("/api/hotels", (req, res) => {
  var city = req.query.city;
  var rating = req.query.rating;

  var and = [];

  if (city != "null" && city != "" && city != undefined) {
    and.push({ city: { name: city } });
  }

  if (rating != "null" && rating != "" && rating != "undefined") {
    and.push({ rating: parseFloat(rating) });
  }

  db.collection("hotels")
    .find(and.length > 0 ? { $and: and } : null)
    .toArray((err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    });
});

app.get("/api/hotels/:id", (req, res) => {
  db.collection("hotels").findOne(
    { _id: ObjectId(req.params.id) },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );
});

app.post("/api/hotels", (req, res) => {
  db.collection("hotels").insertOne(req.body, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(req.body);
    }
  });
});

app.delete("/api/hotels/:id", (req, res) => {
  db.collection("hotels").deleteOne(
    { _id: ObjectId(req.params.id) },
    (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.sendStatus(200);
      }
    }
  );
});

app.post("/api/admin/hotels/insertmany", (req, res) => {
  db.collection("hotels").insertMany(hotels, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(hotels);
    }
  });
});

app.delete("/api/admin/hotels/deleteall", (req, res) => {
  db.collection("hotels").deleteMany({}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.sendStatus(200);
    }
  });
});

// Reservation Area
app.get("/api/reservations", (req, res) => {
  db.collection("reservations")
    .find()
    .toArray((err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    });
});

app.post("/api/reservations", (req, res) => {
  db.collection("reservations").insertOne(req.body, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(req.body);
    }
  });
});

app.get("/api/reservations/:id", (req, res) => {
  db.collection("reservations")
    .find({ userId: { $eq: req.params.id } })
    .toArray((err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    });
});
