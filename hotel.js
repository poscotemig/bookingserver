var hotels = [
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Rio Branco",
    rating: 3,
    city: { name: "Rio Branco" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Maceió",
    rating: 2,
    city: { name: "Maceió" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Macapá",
    rating: 1,
    city: { name: "Macapá" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Manaus",
    rating: 3,
    city: { name: "Manaus" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Salvador",
    rating: 4,
    city: { name: "Salvador" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Fortaleza",
    rating: 5,
    city: { name: "Fortaleza" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Brasília",
    rating: 4,
    city: { name: "Brasília" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Vitória",
    rating: 3,
    city: { name: "Vitória" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Goiânia",
    rating: 2,
    city: { name: "Goiânia" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel São Luís",
    rating: 4,
    city: { name: "São Luís" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Cuiabá",
    rating: 3,
    city: { name: "Cuiabá" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Campo Grande",
    rating: 2,
    city: { name: "Campo Grande" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Belo Horizonte",
    rating: 5,
    city: { name: "Belo Horizonte" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Belém",
    rating: 5,
    city: { name: "Belém" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel João Pessoa",
    rating: 5,
    city: { name: "João Pessoa" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Curitiba",
    rating: 3,
    city: { name: "Curitiba" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Recife",
    rating: 4,
    city: { name: "Recife" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Teresina",
    rating: 2,
    city: { name: "Teresina" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Rio de Janeiro",
    rating: 5,
    city: { name: "Rio de Janeiro" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Natal",
    rating: 3,
    city: { name: "Natal" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Porto Alegre",
    rating: 2,
    city: { name: "Porto Alegre" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Porto Velho",
    rating: 2,
    city: { name: "Porto Velho" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Boa Vista",
    rating: 4,
    city: { name: "Boa Vista" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Florianópolis",
    rating: 3,
    city: { name: "Florianópolis" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel São Paulo",
    rating: 5,
    city: { name: "São Paulo" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Aracaju",
    rating: 4,
    city: { name: "Aracaju" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  },
  {
    image: "https://q-ec.bstatic.com/images/hotel/max1024x768/689/68927928.jpg",
    name: "Hotel Palmas",
    rating: 1,
    city: { name: "Palmas" },
    rooms: [
      { _id: 1, name: "Apartamento Standard", price: 290.7 },
      { _id: 2, name: "Apartamento Luxo", price: 324.9 },
      { _id: 3, name: "Suíte Executiva", price: 453.15 },
      { _id: 4, name: "Suíte Presidencial Sênior", price: 893.73 },
      { _id: 5, name: "Suíte Presidencial Master", price: 1248.23 },
      { _id: 6, name: "Suíte Presidencial Real", price: 2564.78 }
    ]
  }
];

module.exports = hotels;
